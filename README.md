# Authorization service

Service responsible for generating JWT tokens and validating them through separate endpoints.

## Token generation flow

1. Assuming that user is registered in UI with username and password.
2. After log-in, UI makes request to Authorization Service: `POST /token`
    - username & password are passed are request parameters 
3. JWT token is generated and returned to UI.
4. UI stores cookie in browser. 

## Request authorization flow
1. Assuming that valid JWT token cookie is stored in browser.
2. UI makes request to some resource `/R`
    - `cp-user` header is required and stands for user's email
    - call goes through Authorization Service: `/cp-authorization/authorization/R`
3. Authorization Service gets JWT from cookie for given user.
4. JWT signature is validated against user password taken from database.
5. If signature is correct, proxy original request (`/R`) 



# TODO:
1. Tests
2. Connect with real user repository - database underneath