package pl.pwojcik.app.couponstudio.authorization.util;

/**
 * Class aggregating constants related with authorization mechanisms.
 */
public final class AuthorizationConstants {

    /**
     * Constant representing JWT claim for holding current user.
     */
    public static final String JWT_USER_CLAIM = "usr";
}
