package pl.pwojcik.app.couponstudio.authorization.util;

import com.auth0.jwt.algorithms.Algorithm;
import org.bouncycastle.jcajce.provider.digest.SHA3;

import java.time.Instant;

/**
 * Class responsible for providing HMAC secret for JWT Token generation.
 * HMAC secret is seed and time bucket calculated from current timestamp (currently it is 30min window)
 * combined and encoded using SHA3-256 algorithm.
 */
public final class JWTSecretProvider {

    public static final long TOKEN_LIVENESS_IN_MS = 1000 * 60 * 30;

    private final SHA3.Digest256 digest;

    public JWTSecretProvider() {
        digest = new SHA3.Digest256();
    }

    /**
     * Method provides HMAC secret for current time window.
     * @param user username used as seed part
     * @param password password used as seed part
     * @return valid HMAC secret
     */
    public Algorithm createCurrentHMACSecret(final String user, final String password) {
        final long roundedTime = getCurrentTimeRounded();
        return createHMAC(roundedTime, user, password);
    }

    /**
     * Method provides HMAC secret for previous time window.
     * @param user username used as seed part
     * @param password password used as seed part
     * @return valid HMAC secret
     */
    public Algorithm createPreviousHMACSecret(final String user, final String password) {
        final long roundedTime = getCurrentTimeRounded() - TOKEN_LIVENESS_IN_MS;
        return createHMAC(roundedTime, user, password);
    }

    private long getCurrentTimeRounded() {
        final long currentTime = Instant.now().toEpochMilli();
        return (currentTime / TOKEN_LIVENESS_IN_MS) * TOKEN_LIVENESS_IN_MS;
    }

    private Algorithm createHMAC(final long timeBucket, final String user, final String password) {
        final String secret = user + "." + password + "." + timeBucket;
        final byte[] secretHash = digest.digest(secret.getBytes());
        return Algorithm.HMAC256(secretHash);
    }
}
