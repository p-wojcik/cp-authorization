package pl.pwojcik.app.couponstudio.authorization.util;

/**
 * Class containing HTTP headers used within Coupon Studio project.
 */
public final class CouponStudioHttpHeaders {

    /**
     * Header describing tenant making request within system.
     */
    public static final String TENANT_HEADER = "cp-tenant";

    /**
     * Header describing client making the request.
     * As a client we mean service making request to another service.
     */
    public static final String CLIENT_HEADER = "cp-client";

    /**
     * Header describing user making the request.
     */
    public static final String USER_HEADER = "cp-user";
}
