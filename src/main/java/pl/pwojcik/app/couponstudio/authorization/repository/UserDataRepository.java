package pl.pwojcik.app.couponstudio.authorization.repository;

import pl.pwojcik.app.couponstudio.authorization.model.UserCredentials;

/**
 * Interface describing repository of user data (passwords, active tenants, etc).
 */
public interface UserDataRepository {

    /**
     * Retrieves user credentials for given username.
     * @param username email
     * @return user credentials
     */
    UserCredentials getUserCredentials(final String username);

}
