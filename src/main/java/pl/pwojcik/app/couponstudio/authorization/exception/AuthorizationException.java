package pl.pwojcik.app.couponstudio.authorization.exception;

import pl.pwojcik.app.couponstudio.authorization.util.CouponStudioHttpHeaders;

/**
 * Exception indicating that user cannot be authorized due to some reason.
 */
public final class AuthorizationException extends RuntimeException {

    private static final String INVALID_TOKEN_PATTERN = "Given authorization token is invalid: ";
    private static final String USER_HEADER_NOT_PROVIDED = CouponStudioHttpHeaders.USER_HEADER//
            + " header is not provided or is not a valid e-mail address.";
    private static final String COOKIE_NOT_PROVIDED = "Required authorization cookie is not present.";
    private static final String USER_NOT_RECOGNIZED = "User %s cannot be recognized in the system";

    /**
     * Indicates that provided JWT token is invalid
     *
     * @param token provided token
     * @return exception instance
     */
    public static AuthorizationException invalidToken(final String token) {
        return new AuthorizationException(INVALID_TOKEN_PATTERN + token);
    }

    /**
     * Indicates that header cp-user required for Authorization has not been provided or it's empty String.
     *
     * @return exception instance
     */
    public static AuthorizationException userHeaderNotProvided() {
        return new AuthorizationException(USER_HEADER_NOT_PROVIDED);
    }

    /**
     * Indicates that required authorization cookie is not present in user's browser.
     *
     * @return exception instance
     */
    public static AuthorizationException cookieNotProvided() {
        return new AuthorizationException(COOKIE_NOT_PROVIDED);
    }

    /**
     * Indicates that system does not recognize given user.
     *
     * @param user username
     * @return exception instance
     */
    public static AuthorizationException userNotRecognized(final String user) {
        return new AuthorizationException(String.format(USER_NOT_RECOGNIZED, user));
    }

    private AuthorizationException(final String message) {
        super(message);
    }

}
