package pl.pwojcik.app.couponstudio.authorization.handler;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.*;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pwojcik.app.couponstudio.authorization.exception.AuthorizationException;
import pl.pwojcik.app.couponstudio.authorization.model.UserCredentials;
import pl.pwojcik.app.couponstudio.authorization.repository.UserDataRepository;
import pl.pwojcik.app.couponstudio.authorization.util.AuthorizationConstants;
import pl.pwojcik.app.couponstudio.authorization.util.CouponStudioHttpHeaders;
import pl.pwojcik.app.couponstudio.authorization.util.JWTSecretProvider;
import pl.pwojcik.app.couponstudio.authorization.web.Toolbox;

import java.util.Base64;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Handler responsible for validating incoming requests with provided Authorization header.
 * If provided JWT token is valid, request is forwarded to destination service.
 */
public class AuthorizationHandler implements Handler<RoutingContext> {

    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationHandler.class);
    private static final String AUTHORIZATION_COOKIE_NAME_PREFIX = "auth.";
    private static final int CACHE_EXPIRATION_TIME_IN_MIN = 60;

    private final HttpClient restClient;
    private final JWTSecretProvider jwtSecretProvider;
    private final UserDataRepository userDataRepository;
    private final Cache<String, UserCredentials> cache;

    public AuthorizationHandler(final Toolbox toolbox,//
                                final JWTSecretProvider jwtSecretProvider,//
                                final UserDataRepository userDataRepository) {
        this.restClient = toolbox.getRestClient();
        this.jwtSecretProvider = jwtSecretProvider;
        this.userDataRepository = userDataRepository;

        this.cache = CacheBuilder.newBuilder()//
                .expireAfterAccess(CACHE_EXPIRATION_TIME_IN_MIN, TimeUnit.MINUTES)//
                .softValues()//
                .build();
    }

    @Override
    public void handle(final RoutingContext context) {
        final HttpServerRequest source = context.request();
        final HttpServerResponse serverResponse = context.response();
        try {
            LOG.debug("Fetching header {} ", CouponStudioHttpHeaders.USER_HEADER);
            final String userHeader = source.getHeader(CouponStudioHttpHeaders.USER_HEADER);
            if (isBlank(userHeader) || !EmailValidator.getInstance().isValid(userHeader)) {
                throw AuthorizationException.userHeaderNotProvided();
            }

            LOG.debug("Fetching authorization cookie for user {}", userHeader);
            final Cookie cookie = getCookieForUser(context, userHeader);
            if (isNull(cookie) || isBlank(cookie.getValue())) {
                throw AuthorizationException.cookieNotProvided();
            }

            LOG.debug("Fetching credentials for given username.", userHeader);
            final UserCredentials userCredentials = getCredentials(userHeader);
            if (isNull(userCredentials)) {
                throw AuthorizationException.userNotRecognized(userHeader);
            }

            final String jwtToken = cookie.getValue();

            LOG.debug("Validating JWT token {}", jwtToken);
            final DecodedJWT decodedJWT = verifyTokenWithRetry(jwtToken, userCredentials);
            if (!areClaimsValid(userCredentials, decodedJWT)) {
                throw AuthorizationException.invalidToken(jwtToken);
            }

            final String absoluteURI = prepareDestinationRequestURI(source);
            LOG.debug("JWT is valid, proxying request: {} {}", source.method().name(), absoluteURI);
            final HttpClientRequest clientRequest = restClient.requestAbs(source.method(), absoluteURI,//
                    clientResponse -> rewriteClientResponse(serverResponse, clientResponse));

            if (isNotBlank(userCredentials.getActiveTenant())) {
                clientRequest.putHeader(CouponStudioHttpHeaders.TENANT_HEADER, userCredentials.getActiveTenant());
            }
            clientRequest.putHeader(CouponStudioHttpHeaders.CLIENT_HEADER, decodedJWT.getIssuer());
            clientRequest.putHeader(CouponStudioHttpHeaders.USER_HEADER, userCredentials.getEmail());

            // Rewrite payload
            source.handler(clientRequest::write);
            source.endHandler((v) -> clientRequest.end());
        } catch (final AuthorizationException exception) {
            LOG.info("Rejected request due to exception: {}", exception.getMessage());
            serverResponse.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code());
            serverResponse.end();
        }
    }

    private Cookie getCookieForUser(final RoutingContext context, final String user) {
        final String cookieNameDecoded = AUTHORIZATION_COOKIE_NAME_PREFIX + "." + user;
        final String cookieNameEncoded = Base64.getEncoder().encodeToString(cookieNameDecoded.getBytes());
        return context.getCookie(cookieNameEncoded);
    }

    private UserCredentials getCredentials(final String user) {
        try {
            return cache.get(user, () -> userDataRepository.getUserCredentials(user));
        } catch (final ExecutionException e) {
            LOG.error("Unexpected error occurred while fetching user credentials. ", e);
            return null;
        }
    }

    private String prepareDestinationRequestURI(final HttpServerRequest source) {
        final String destinationURI = "/" + source.getParam("param0");
        final int hostEndIndex = source.absoluteURI().indexOf(source.uri());
        final String host = source.absoluteURI().substring(0, hostEndIndex);
        return host + destinationURI;
    }

    private boolean isUserMatching(final String username, final Claim claim) {
        return username.equalsIgnoreCase(claim.asString());
    }

    private boolean isTenantMatching(final String activeTenant, final String jwtTenant) {
        if (isBlank(activeTenant)) {
            return true;
        }
        return Objects.equals(activeTenant, jwtTenant);
    }

    private boolean areClaimsValid(final UserCredentials userCredentials, final DecodedJWT decodedJWT) {
        final boolean validClient = isNotBlank(decodedJWT.getIssuer());
        final boolean validUser = isUserMatching(userCredentials.getEmail(),
                decodedJWT.getClaim(AuthorizationConstants.JWT_USER_CLAIM));
        final boolean validTenant = isTenantMatching(userCredentials.getActiveTenant(), decodedJWT.getSubject());
        return validClient && validUser && validTenant;
    }

    private void rewriteClientResponse(final HttpServerResponse serverResponse, final HttpClientResponse clientResponse) {
        serverResponse.headers().setAll(clientResponse.headers());
        final int contentLength = Integer.parseInt(//
                ofNullable(clientResponse.getHeader(HttpHeaders.CONTENT_LENGTH)).orElse("256"));
        final Buffer body = Buffer.buffer(contentLength);
        clientResponse.handler(body::appendBuffer);
        clientResponse.endHandler((v) -> {
            LOG.info("Received client response with status code {} for request: {}", //
                    clientResponse.statusCode(), clientResponse.request().absoluteURI());
            serverResponse.setStatusCode(clientResponse.statusCode());
            serverResponse.headers().set(HttpHeaders.CONTENT_LENGTH, String.valueOf(contentLength));
            serverResponse.write(body);
            serverResponse.end();
        });
    }

    private DecodedJWT verifyTokenWithRetry(final String token, final UserCredentials credentials) {
        try {
            final Algorithm currentSecret = jwtSecretProvider.createCurrentHMACSecret(//
                    credentials.getEmail(), credentials.getPassword());
            return verifyToken(currentSecret, token);
        } catch (final JWTVerificationException e) {
            try {
                final Algorithm previousSecret = jwtSecretProvider.createPreviousHMACSecret(//
                        credentials.getEmail(), credentials.getPassword());
                return verifyToken(previousSecret, token);
            } catch (final JWTVerificationException ex) {
                throw AuthorizationException.invalidToken(token);
            }
        }
    }

    private DecodedJWT verifyToken(final Algorithm algorithm, final String token) {
        final JWTVerifier verifier = JWT.require(algorithm).build();
        return verifier.verify(token);
    }

}
