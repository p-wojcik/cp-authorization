package pl.pwojcik.app.couponstudio.authorization.exception;

import pl.pwojcik.app.couponstudio.authorization.util.CouponStudioHttpHeaders;

/**
 * Exception indicating that some error occurred during JWT token generation.
 */
public final class TokenGenerationException extends RuntimeException {

    private static final String CREDENTIALS_NOT_PROVIDED = "Both query parameters 'username' and " //
            + "'password' have not been provided.";
    private static final String CLIENT_HEADER_NOT_PROVIDED = CouponStudioHttpHeaders.CLIENT_HEADER//
            + " header is not provided or it is empty String.";
    private static final String CREDENTIALS_NOT_VALID = "Credentials provided through query parameters: " //
            + "'username' and 'password' are not valid.";

    /**
     * Indicates that header cp-client required for token generation has not been provided or it's empty String.
     *
     * @return exception instance
     */
    public static TokenGenerationException clientHeaderNotProvided() {
        return new TokenGenerationException(CLIENT_HEADER_NOT_PROVIDED);
    }

    /**
     * Indicates that query parameters 'username' & 'password' has not been provided.
     *
     * @return exception instance
     */
    public static TokenGenerationException credentialsNotProvided() {
        return new TokenGenerationException(CREDENTIALS_NOT_PROVIDED);
    }

    /**
     * Indicates that credentials provided through query parameters are not valid.
     * @return exception instance
     */
    public static TokenGenerationException credentialsNotValid() {
        return new TokenGenerationException(CREDENTIALS_NOT_VALID);
    }

    private TokenGenerationException(final String message) {
        super(message);
    }
}
