package pl.pwojcik.app.couponstudio.authorization.config;

/**
 * Class responsible for aggregating startup & configuration parameters necessary to run application.
 */
public final class ConfigurationProvider
{
	private static final int LISTEN_PORT = 8083;
	private static final String APPLICATION_ROOT_PATH = "/cp-authorization";

	/**
	 * Port on which application is accepting requests.
	 * @return listening port
	 */
	public int getListenPort()
	{
		return LISTEN_PORT;
	}

	/**
	 * Application path prefix.
	 * @return application prefix
	 */
	public String getApplicationRootPath()
	{
		return APPLICATION_ROOT_PATH;
	}

}
