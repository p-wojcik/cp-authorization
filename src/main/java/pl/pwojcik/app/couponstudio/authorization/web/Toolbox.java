package pl.pwojcik.app.couponstudio.authorization.web;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import pl.pwojcik.app.couponstudio.authorization.config.ConfigurationProvider;

/**
 * Holder for instances shared among organization/location domains.
 */
public final class Toolbox
{
	private final Vertx vertx;
	private final HttpClient restClient;
	private final ConfigurationProvider configurationProvider;

	Toolbox()
	{
		this.vertx = Vertx.vertx();
		this.configurationProvider = new ConfigurationProvider();
		this.restClient = vertx.createHttpClient();
	}

	/**
	 * Returns VertX instance.
	 * @return vertx instance
	 */
	Vertx getVertx()
	{
		return vertx;
	}

	/**
	 * Returns wrapper for configuration parameters.
	 * @return configuration provider
	 */
	ConfigurationProvider getConfigurationProvider() {
		return configurationProvider;
	}

	/**
	 * Returns REST client used to make external calls.
	 * @return REST client
	 */
	public HttpClient getRestClient() {
		return restClient;
	}

}
