package pl.pwojcik.app.couponstudio.authorization.web;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CookieHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pwojcik.app.couponstudio.authorization.config.ConfigurationProvider;
import pl.pwojcik.app.couponstudio.authorization.handler.AuthorizationHandler;
import pl.pwojcik.app.couponstudio.authorization.handler.FailureHandler;
import pl.pwojcik.app.couponstudio.authorization.handler.TokenRequestHandler;
import pl.pwojcik.app.couponstudio.authorization.repository.DummyUserDataRepository;
import pl.pwojcik.app.couponstudio.authorization.repository.UserDataRepository;
import pl.pwojcik.app.couponstudio.authorization.util.JWTSecretProvider;

/**
 * Class responsible for setting up whole application.
 */
public class ApplicationInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationInitializer.class);
    private static final String AUTHORIZATION_ENDPOINT_PATH = "/authorization/(.*)";
    private static final String TOKEN_ENDPOINT_PATH = "/token";

    public static void main(final String[] args) {
        final Toolbox toolbox = new Toolbox();
        final Vertx vertx = toolbox.getVertx();
        final ConfigurationProvider configurationProvider = toolbox.getConfigurationProvider();

        final HttpServer httpServer = vertx.createHttpServer();
        final Router router = Router.router(vertx);
        final Router subRouter = Router.router(vertx);
        router.mountSubRouter(configurationProvider.getApplicationRootPath(), subRouter);

        provideRoutes(subRouter, toolbox);

        httpServer.requestHandler(router::accept)//
                .listen(configurationProvider.getListenPort(),
                        avoid -> LOG.info("Server is up on port " + configurationProvider.getListenPort()));
    }

    private static void provideRoutes(final Router router, final Toolbox toolbox) {
        LOG.info("Initializing authorization route: {}", AUTHORIZATION_ENDPOINT_PATH);
        final JWTSecretProvider jwtSecretProvider = new JWTSecretProvider();
        final UserDataRepository userDataRepository = new DummyUserDataRepository();
        final AuthorizationHandler authorizationHandler = new AuthorizationHandler(toolbox, jwtSecretProvider, userDataRepository);
        router.routeWithRegex(AUTHORIZATION_ENDPOINT_PATH).handler(CookieHandler.create());
        router.routeWithRegex(AUTHORIZATION_ENDPOINT_PATH).handler(authorizationHandler);

        LOG.info("Initializing token request route for HTTP method {}: {}", HttpMethod.POST.name(), TOKEN_ENDPOINT_PATH);
        final TokenRequestHandler tokenRequestHandler = new TokenRequestHandler(jwtSecretProvider, userDataRepository);
        router.route(HttpMethod.POST, TOKEN_ENDPOINT_PATH).handler(CookieHandler.create());
        router.route(HttpMethod.POST, TOKEN_ENDPOINT_PATH).handler(tokenRequestHandler);

        LOG.info("Initializing non-mapped routes handler.");
        router.routeWithRegex(".*")//
                .handler(event -> event.response().setStatusCode(HttpResponseStatus.NOT_FOUND.code()).end());

        LOG.info("Initializing failure handler.");
        router.route().failureHandler(new FailureHandler());
    }
}

