package pl.pwojcik.app.couponstudio.authorization.handler;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pwojcik.app.couponstudio.authorization.exception.AuthorizationException;
import pl.pwojcik.app.couponstudio.authorization.exception.TokenGenerationException;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * Handler responsible for alleviating unexpected exceptions.
 */
public class FailureHandler implements Handler<RoutingContext> {

    private static final Logger LOG = LoggerFactory.getLogger(FailureHandler.class);

    @Override
    public void handle(final RoutingContext event) {
        final HttpServerRequest request = event.request();
        final HttpServerResponse response = request.response();
        final String requestURI = request.uri();
        final int statusCode = event.statusCode();

        final Throwable cause = event.failure();
        final String message = cause.getMessage();
        final Optional<Integer> processedCause = processException(cause);
        if (processedCause.isPresent()) {
            LOG.warn("Rejecting request {} with status code {} and message {}", requestURI, statusCode, message);
            response.setStatusCode(processedCause.get())
                    .setStatusMessage(message)//
                    .end();
        } else {
            LOG.error("Internal error for route {}, status {}, details {} ", requestURI, statusCode, event.failure());
            response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()) //
                    .setStatusMessage("Internal error for route: " + requestURI)//
                    .end();
        }
    }

    private Optional<Integer> processException(final Throwable cause) {
        if (cause instanceof AuthorizationException) {
            return of(HttpResponseStatus.UNAUTHORIZED.code());
        }
        if (cause instanceof TokenGenerationException) {
            return of(HttpResponseStatus.BAD_REQUEST.code());
        }
        return empty();
    }
}
