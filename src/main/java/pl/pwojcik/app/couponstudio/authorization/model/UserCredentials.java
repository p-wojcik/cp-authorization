package pl.pwojcik.app.couponstudio.authorization.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import static java.util.Objects.isNull;

/**
 * Class representing user credentials.
 */
public final class UserCredentials {

    private final String email;
    private final String password;
    private final String activeTenant;

    @JsonCreator
    public UserCredentials(@JsonProperty("email") final String email,//
                           @JsonProperty("password") final String password,//
                           @JsonProperty("activeTenant") final String activeTenant) {
        this.email = email;
        this.password = password;
        this.activeTenant = activeTenant;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getActiveTenant() {
        return activeTenant;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)//
                .append("email", email)//
                .append("password", "<hidden password>")//
                .append("activeTenant", activeTenant)//
                .build();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()//
                .append(email)//
                .append(password)//
                .append(activeTenant)//
                .build();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (isNull(obj) || !(obj instanceof UserCredentials)) {
            return false;
        }

        final UserCredentials that = (UserCredentials) obj;
        return new EqualsBuilder()//
                .append(email, that.email)//
                .append(password, that.password)//
                .append(activeTenant, that.activeTenant)//
                .build();
    }
}
