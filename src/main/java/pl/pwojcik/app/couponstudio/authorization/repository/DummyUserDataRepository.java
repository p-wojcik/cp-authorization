package pl.pwojcik.app.couponstudio.authorization.repository;

import pl.pwojcik.app.couponstudio.authorization.model.UserCredentials;

/**
 * Dummy implementation of {@link UserDataRepository} interface.
 */
public class DummyUserDataRepository implements UserDataRepository {

    @Override
    public UserCredentials getUserCredentials(String username) {
        return new UserCredentials(
                "kasztan@aaa.pl", "pawel", "bober"
        );
    }
}
