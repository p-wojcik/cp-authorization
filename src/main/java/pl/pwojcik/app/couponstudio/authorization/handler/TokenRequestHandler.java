package pl.pwojcik.app.couponstudio.authorization.handler;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.pwojcik.app.couponstudio.authorization.exception.TokenGenerationException;
import pl.pwojcik.app.couponstudio.authorization.model.UserCredentials;
import pl.pwojcik.app.couponstudio.authorization.repository.UserDataRepository;
import pl.pwojcik.app.couponstudio.authorization.util.CouponStudioHttpHeaders;
import pl.pwojcik.app.couponstudio.authorization.util.JWTSecretProvider;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static pl.pwojcik.app.couponstudio.authorization.util.AuthorizationConstants.JWT_USER_CLAIM;
import static pl.pwojcik.app.couponstudio.authorization.util.CouponStudioHttpHeaders.CLIENT_HEADER;
import static pl.pwojcik.app.couponstudio.authorization.util.JWTSecretProvider.TOKEN_LIVENESS_IN_MS;

/**
 * Handler responsible for generating JWT token for given user credentials.
 * JWT token contains user name & default user's tenant if it already exists.
 */
public class TokenRequestHandler implements Handler<RoutingContext> {

    private static final Logger LOG = LoggerFactory.getLogger(TokenRequestHandler.class);
    private static final String USERNAME_PARAM = "username";
    private static final String PASSWORD_PARAM = "password";

    private final JWTSecretProvider jwtSecretProvider;
    private final UserDataRepository userDataRepository;

    public TokenRequestHandler(final JWTSecretProvider jwtSecretProvider, final UserDataRepository userDataRepository) {
        this.jwtSecretProvider = jwtSecretProvider;
        this.userDataRepository = userDataRepository;
    }

    @Override
    public void handle(final RoutingContext event) {
        final HttpServerRequest request = event.request();
        final HttpServerResponse response = event.response();
        final String clientName = request.getHeader(CLIENT_HEADER);

        LOG.debug("Fetching header {}", CouponStudioHttpHeaders.CLIENT_HEADER);
        if (isBlank(clientName)) {
            throw TokenGenerationException.clientHeaderNotProvided();
        }

        LOG.debug("Validating presence of query parameters '{}' and '{}'", USERNAME_PARAM, PASSWORD_PARAM);
        final Optional<String> optionalUsername = getDecodedQueryParam(request, USERNAME_PARAM);
        final Optional<String> optionalPassword = getDecodedQueryParam(request, PASSWORD_PARAM);
        if (!optionalUsername.isPresent() || !optionalPassword.isPresent()) {
            throw TokenGenerationException.credentialsNotProvided();
        }

        final String username = optionalUsername.get();
        final String password = optionalPassword.get();
        LOG.debug("Validating provided username.");
        if (!EmailValidator.getInstance().isValid(username)) {
            throw TokenGenerationException.credentialsNotValid();
        }

        LOG.debug("Checking user credentials.");
        final UserCredentials credentials = userDataRepository.getUserCredentials(username);
        if (isNull(credentials) || !password.equals(credentials.getPassword())) {
            throw TokenGenerationException.credentialsNotValid();
        }

        LOG.debug("Generating JWT token for user {}", username);
        final Algorithm currentHMACSecret = jwtSecretProvider.createCurrentHMACSecret(username, password);
        final Instant now = Instant.now();

        final Date currentDate = new Date(now.toEpochMilli());
        final Date expiresAt = new Date(now.plus(TOKEN_LIVENESS_IN_MS, ChronoUnit.MILLIS).toEpochMilli());

        final JWTCreator.Builder jwtBuilder = JWT.create()//
                .withIssuer(clientName)//
                .withIssuedAt(currentDate)//
                .withExpiresAt(expiresAt)//
                .withClaim(JWT_USER_CLAIM, username);

        if (nonNull(credentials.getActiveTenant())) {
            // If user has default tenant chosen, add it to JWT
            jwtBuilder.withSubject(credentials.getActiveTenant());
        }

        final String jwtToken = jwtBuilder.sign(currentHMACSecret);
        response.end(jwtToken);
    }

    private Optional<String> getDecodedQueryParam(final HttpServerRequest request, final String paramName) {
        final String paramValue = request.getParam(paramName);
        if (StringUtils.isNotBlank(paramValue)) {
            final byte[] base64DecodedParam = Base64.getDecoder().decode(paramValue);
            return Optional.of(new String(base64DecodedParam));
        }
        return Optional.empty();
    }
}
